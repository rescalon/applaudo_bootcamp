package com.studios.applaudo.applaudohomework.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import com.studios.applaudo.applaudohomework.R;
import com.studios.applaudo.applaudohomework.TeamListenerInterface;
import com.studios.applaudo.applaudohomework.fragment.TeamDetailFragment;
import com.studios.applaudo.applaudohomework.fragment.TeamListFragment;
import com.studios.applaudo.applaudohomework.model.TeamModel;

/**
 * Created by Robert on 6/24/2016.
 */
public class MainActivity2 extends AppCompatActivity implements TeamListenerInterface{

    public final static String PASSPARCELABLE = "parcelObjects";

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TeamListFragment frgList = (TeamListFragment)getSupportFragmentManager().findFragmentById(R.id.frgTeamList);
        frgList.setListener(this);
    }

    @Override
    public void onTeamSelected(TeamModel team) {

        //creating an object refered to the fragmentDetal
        TeamDetailFragment frgDetail = (TeamDetailFragment)getSupportFragmentManager().findFragmentById(R.id.frgDetail);
        boolean okDetail = (frgDetail !=null && frgDetail.isVisible());

        if (okDetail){
            frgDetail.showTeamDetail(team);
            /* If fragment is not null it will display the detail fragment */
        } else{
            Intent openDetail = new Intent(MainActivity2.this, TeamDetailActivity2.class);
            openDetail.putExtra(PASSPARCELABLE, team);
            startActivity(openDetail);
            /* But if we do not have the fragment in the same view, we will display the data
            * in another activity*/
        }
    }
}
