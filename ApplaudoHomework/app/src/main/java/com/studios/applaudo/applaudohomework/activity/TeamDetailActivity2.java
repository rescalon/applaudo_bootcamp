package com.studios.applaudo.applaudohomework.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import com.studios.applaudo.applaudohomework.R;
import com.studios.applaudo.applaudohomework.fragment.TeamDetailFragment;
import com.studios.applaudo.applaudohomework.model.TeamModel;

/**
 * Created by Robert on 6/24/2016.
 */
public class TeamDetailActivity2 extends AppCompatActivity {

    TeamModel teamModel;
    private String mPhoneNumber,mStadium;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_team_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle bundle = getIntent().getExtras();
        teamModel = bundle.getParcelable(MainActivity2.PASSPARCELABLE);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(teamModel.getTeam_name());
        }

        mPhoneNumber = teamModel.getPhone_number();
        mStadium = teamModel.getStadium();

        TeamDetailFragment detail = (TeamDetailFragment)getSupportFragmentManager().findFragmentById(R.id.frgDetail);
        detail.showTeamDetail((TeamModel)getIntent().getParcelableExtra(MainActivity2.PASSPARCELABLE));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail_action_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            // IN CASE PHONE ICON IS PRESSED
            case R.id.ic_call_id:
                Uri uriPhone = Uri.parse("tel: "+mPhoneNumber);
                Intent makePhoneCall = new Intent(Intent.ACTION_CALL, uriPhone);
                startActivity(makePhoneCall);
                Toast.makeText(TeamDetailActivity2.this, "CALLING TO: " + mPhoneNumber, Toast.LENGTH_LONG).show();
                break;
            // IN CASE SHARE ICON IS PRESSED
            case R.id.ic_share_id:
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, mStadium);
                startActivity(Intent.createChooser(shareIntent,"SHARE"));
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
