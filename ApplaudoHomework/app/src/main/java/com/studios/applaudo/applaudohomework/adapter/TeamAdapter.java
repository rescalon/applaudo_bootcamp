package com.studios.applaudo.applaudohomework.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.studios.applaudo.applaudohomework.R;
import com.studios.applaudo.applaudohomework.model.TeamModel;
import java.util.List;
/**
 * Created by Robert on 6/22/2016.
 */
public class TeamAdapter extends RecyclerView.Adapter<TeamAdapter.MyViewHolder> {

    Context context;
    public interface OnItemClickListener {
        void onItemClick(TeamModel item);
    }

    private List<TeamModel> mTeamList; /* Creating the object for the list*/
    private OnItemClickListener mListener; /* Listener for each item */

    public TeamAdapter(Context context, List<TeamModel> mTemList, OnItemClickListener mListener){
        this.context = context;
        this.mListener = mListener;
        this.mTeamList = mTemList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View items = LayoutInflater.from(parent.getContext()).inflate(R.layout.team_list_row, null, false);
        return new MyViewHolder(items);
        /* inflating the view */
    }

    static class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView mTeamName,mTeamAdress;
        private ImageView mLogo;

        public MyViewHolder(View itemView) {
            super(itemView);
            mTeamName = (TextView)itemView.findViewById(R.id.teamNameID);
            mTeamAdress = (TextView)itemView.findViewById(R.id.addressID);
            mLogo = (ImageView) itemView.findViewById(R.id.imgLogoID);
        }

        public void bind(final TeamModel item, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        TeamModel model = mTeamList.get(position);
        holder.mTeamName.setText(model.getTeam_name());
        holder.mTeamAdress.setText(model.getAddress());
        Glide.with(context).load(model.getImg_logo()).placeholder(R.drawable.loading).error(R.drawable.error).into(holder.mLogo);
        holder.bind(mTeamList.get(position), mListener);
        /*Setting data into textviews */
    }

    @Override
    public int getItemCount() {
        return mTeamList.size();
    }
}
