package com.studios.applaudo.applaudohomework.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.studios.applaudo.applaudohomework.R;
import com.studios.applaudo.applaudohomework.TeamApiService;
import com.studios.applaudo.applaudohomework.TeamListenerInterface;
import com.studios.applaudo.applaudohomework.adapter.TeamAdapter;
import com.studios.applaudo.applaudohomework.model.TeamModel;
import java.util.List;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Robert on 6/24/2016.
 */
public class TeamListFragment extends Fragment {

    private TeamListenerInterface mteamListener;
    private RecyclerView mLvTeams;
    TeamApiService teamApiService = new TeamApiService();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        return inflater.inflate(R.layout.team_list_recycler_view, container, false);
    }

    @Override
    public void onActivityCreated(Bundle state){
        super.onActivityCreated(state);
        if (getView() != null) {
            mLvTeams =(RecyclerView)getView().findViewById(R.id.recycleView_team);
            getTeamList();
        }
    }

    public void getTeamList(){
        /*Calling the method that instantiates the interface*/
        teamApiService.getAdapter().getTeam(new Callback<List<TeamModel>>() {
            //IN CASE WE GET RESPONSE
            @Override
            public void success(final List<TeamModel> mTeamList, Response response) {
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                mLvTeams.setLayoutManager(mLayoutManager);
                mLvTeams.setAdapter(new TeamAdapter(getActivity(), mTeamList, new TeamAdapter.OnItemClickListener() {
                    /*Passing parameters using parcelable when item is clicked*/
                    @Override
                    public void onItemClick(TeamModel item) {
                            if (mteamListener!=null){
                                mteamListener.onTeamSelected(item);
                            }
                       }
                }));
            }
            //IN CASE WE DO NOT GET RESPONSE
            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getActivity(), "ERROR", Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void setListener(TeamListenerInterface litener){
        this.mteamListener = litener;
    }
}
