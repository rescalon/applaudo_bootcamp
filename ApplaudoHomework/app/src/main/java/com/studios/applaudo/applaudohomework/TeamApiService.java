package com.studios.applaudo.applaudohomework;

import retrofit.RestAdapter;

/**
 * Created by Robert on 6/22/2016.
 */
public class TeamApiService{

    private TeamInterfaceService teamInterfaceService;

    public  TeamApiService(){
        String mURL = "http://applaudostudios.com";
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(mURL).build();
        teamInterfaceService = restAdapter.create(TeamInterfaceService.class);
        /*Creating rest adapter and setting end point to get JSON API*/
    }

    public TeamInterfaceService getAdapter(){
        return teamInterfaceService;
        /*Method that istantiates the interface*/
    }
 }
