package com.studios.applaudo.applaudohomework;

import com.studios.applaudo.applaudohomework.model.TeamModel;
import java.util.List;
import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by Robert on 6/20/2016.
 */
public interface TeamInterfaceService {
    // RETROFIT GET INTERFACE
    @GET("/external/applaudo_homework.json")
    void getTeam(Callback<List<TeamModel>> callback); //METHOD TO MAKE A CALLBACK TO THE LIST WITH THE TeamModel VARIABLES
}
