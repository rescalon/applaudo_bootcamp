package com.studios.applaudo.applaudohomework.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Robert on 6/20/2016.
 */
public class TeamModel implements Parcelable{

    private String team_name;
    private String address;
    private String description;
    private String img_logo;
    private String video_url;
    private String phone_number;
    private String stadium;

    public String getTeam_name() {
        return team_name;
    }

    public String getAddress() {
        return address;
    }

    public String getDescription() {
        return description;
    }

    public String getImg_logo() {
        return img_logo;
    }

    public String getVideo_url() {
        return video_url;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public String getStadium() {
        return stadium;
    }

    //Parcelable Method
    public static final Parcelable.Creator<TeamModel> CREATOR = new Creator<TeamModel>() {
        @Override
        public TeamModel createFromParcel(Parcel source) {
            TeamModel teamModel = new TeamModel();
            teamModel.team_name = source.readString();
            teamModel.address = source.readString();
            teamModel.description = source.readString();
            teamModel.img_logo = source.readString();
            teamModel.video_url = source.readString();
            teamModel.phone_number = source.readString();
            teamModel.stadium = source.readString();

            return teamModel;
        }

        @Override
        public TeamModel[] newArray(int size) {
            return new TeamModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(team_name);
        parcel.writeString(address);
        parcel.writeString(description);
        parcel.writeString(img_logo);
        parcel.writeString(video_url);
        parcel.writeString(phone_number);
        parcel.writeString(stadium);
    }
}
