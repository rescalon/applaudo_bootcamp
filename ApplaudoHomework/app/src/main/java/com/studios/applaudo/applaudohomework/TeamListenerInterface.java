package com.studios.applaudo.applaudohomework;

import com.studios.applaudo.applaudohomework.model.TeamModel;

/**
 * Created by Robert on 6/24/2016.
 */
public interface TeamListenerInterface {
    void onTeamSelected(TeamModel team);
}
