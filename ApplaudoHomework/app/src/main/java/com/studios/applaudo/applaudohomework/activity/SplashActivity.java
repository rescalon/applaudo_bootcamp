package com.studios.applaudo.applaudohomework.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import com.studios.applaudo.applaudohomework.R;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Robert on 6/22/2016.
 */
public class SplashActivity extends AppCompatActivity{

    private static final long SPLASH_TIMER = 3000; // SET TIME FOR THE SPLASH DURATION

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
        }

        TimerTask myTask = new TimerTask() {
            @Override
            public void run() {
                Intent openMainActivity = new Intent(SplashActivity.this, MainActivity2.class);
                startActivity(openMainActivity);
                finish();
            }
        };
        Timer timer = new Timer();
        timer.schedule(myTask,SPLASH_TIMER);
    }
}
