package com.studios.applaudo.applaudohomework.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;
import com.bumptech.glide.Glide;
import com.studios.applaudo.applaudohomework.R;
import com.studios.applaudo.applaudohomework.model.TeamModel;

public class TeamDetailFragment extends Fragment{

    private VideoView mteamVideo;
    public Button mbtnPlay, mbtnPause;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        return inflater.inflate(R.layout.activity_team_detail,container,false);
    }

    public void showTeamDetail(TeamModel team){
        if (getView() != null) {
            TextView mteamName = (TextView) getView().findViewById(R.id.team_name_id);
            TextView mteamDesc = (TextView) getView().findViewById(R.id.team_desc_id);
            ImageView mteamLogo = (ImageView) getView().findViewById(R.id.team_logo_id);
            mteamVideo = (VideoView)getView().findViewById(R.id.team_video_id);
            mteamVideo = (VideoView)getView().findViewById(R.id.team_video_id);
            mbtnPlay = (Button)getView().findViewById(R.id.btnPlay);
            mbtnPause = (Button)getView().findViewById(R.id.btnPause);

            mteamName.setText(team.getTeam_name());
            mteamDesc.setText(team.getDescription());

            Glide.with(this).load(team.getImg_logo()).placeholder(R.drawable.loading).error(R.drawable.error).into(mteamLogo);

            // GET EXTRA FROM VIDEO
            String videoURL = "http://clips.vorwaerts-gmbh.de/VfE_html5.mp4";
            Uri uri = Uri.parse(videoURL);
            mteamVideo.setVideoURI(uri);
            mteamVideo.setBackground(getActivity().getDrawable(R.drawable.video_placeholder2));

            mbtnPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mteamVideo.start();
                    mteamVideo.setBackground(null);
                }
            });

            mbtnPause.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mteamVideo.pause();
                }
            });
        }
    }
}
